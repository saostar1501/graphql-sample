using HandicraftShop.Catalog.API.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

var configurations = builder.Configuration;
var devOptions = configurations.GetSection("DevOptions").Get<DevOptions>();

if(devOptions.EnableSwagger)
    builder.Services.AddSwaggerGen();

if(devOptions.EnableCors)
    builder.Services.AddCors(options =>
    {
        options.AddPolicy("CorsPolicy", builder => builder
            .WithOrigins("http://localhost:4200")
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials());
    });


var app = builder.Build();

// Configure the HTTP request pipeline.
if (devOptions.EnableSwagger)
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

if (devOptions.EnableCors)
{
    app.UseCors("CorsPolicy");
}

app.UseAuthorization();

app.MapControllers();

app.Run();
